### CANDIDATE ELIMINATION AND ID3-IMPLEMENTATION
*This project uses 3 ~~basic~~ important algorithms in the world of Machine Learning*

1. Candidate Elimination
    - Candidate Elimination uses the principle of Concept Hypothesis. The Version Space defines the area within which the hypothesis lies.
    - The target hypothesis if exists in the dataset then gives us a good result for our requirements
      
2. Id3 Algorithm
    - Classifies Instances by sorting them down the tree from root to some leaf node. Approximates Discrete Valued Targets!
    - Errors in Data will give wrong results
3. [Random Forest](https://bitbucket.org/machinelearning11/randomforest)
    - Make Random Set of Trees and then perform ID3 and Calculate the best of all using the test Data that you have
    - Useful but Time Consuming. Can be used as an Inital Approach to understand the Dataset
    
#### RESULTS

| Algorithm        | Ratio           | Accuracy  |
| ------------- |:-------------:| -----:|
| Id3-Algorithm      | 12253/16281 | 75.3 |
| Random Forest     | **DNF**      |   77.2 |


**For Candidate Elimination check the text Files for Result**

[Output](https://bitbucket.org/machinelearning11/id3_ce/src/a9f4759050e01b5b93bdcfdaf5f24c75d4564f09/logs/candidateElimination_1506961955198.txt?at=master&fileviewer=file-view-default)


**For ID3 download the log for Detailed Analysis of Result**

[Output](https://bitbucket.org/machinelearning11/id3_ce/raw/a9f4759050e01b5b93bdcfdaf5f24c75d4564f09/logs/id3Implementation_1506492303186.txt)

### EXECUTION PROCEDURE
-----------------------------------
- Install java in your system. 
- Type java -version to test if it is installed
- Assuming you are in the out directory after cloning the repository. Type the following commands in the command
    1. cd  production/projectSets/
    2. java com.company.Main



