package Common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class GeneralUse {
	/**
	 * Returns Data by Parameters
	 * @param list List of Data
	 * @param variable The Variable Group to search for
	 * @param typeVal The value for each OBject Type
	 * @param <E> The new_List formed with new_data
	 * @return ArrayList of Generic Type
	 */
	public <E> ArrayList<E> selectIntDataByParam(ArrayList<E> list, String variable, int typeVal)
	{
		ArrayList<E> new_list = new ArrayList<>();
		for (E each_mem: list)
		{
			Class<?> cname = each_mem.getClass();
			try {
				Field requiredType = cname.getField(variable);
				int res = requiredType.getInt(each_mem);
				if (typeVal == res) {
					new_list.add(each_mem);
				}
			} catch (Exception e)
			{
				System.out.println("No Such Field found: " + variable);
			}
		}
		return new_list;
	}

	/**
	 * Writing an array to logs
	 * @param file Log FIle Wrirting
	 * @param list List of Strings to Write
	 * @param len Length of data
	 * @param firstLine the first line in the String
	 * @throws IOException Hadling IO Errors
	 */
	public void writeSimpleArrayOfStrings(File file, String[] list, int len, String firstLine) throws IOException
	{
		writeToLogs(file, "*** " + firstLine + " ***");
		String safe = "";
		for (int i = 0; i< len; i++)
		{
			safe = safe.concat(list[i] + "\t");
		}
		writeToLogs(file, safe);
	}
	public void writeSimpleArrayListOfStrings(File file, ArrayList<String[]> list, int len, String firstLine) throws IOException
	{
		writeToLogs(file, "*** " + firstLine + " ***");
		ArrayList<String []> new_list = new ArrayList<>();
		for (String[] s: list)
		{
			int leng = new_list.size(), k = 0;
			boolean contains = false;
			for (String p[]: new_list)
			{
				for (k = 0; k < 16; k++) {
					if (!s[k].equals(p[k])) {break;}
				}
				if (k == 16) {contains = true; break;}
			}
			if (!contains) {
				new_list.add(s);
			}
		}
		for (String[] s: new_list)
		{
			String safe = "";
			for (int j = 0; j < 16; j++)
			{
				safe = safe.concat(s[j] + "\t");
			}
			writeToLogs(file, safe + "\n");
		}
	}

	public void writeSimpleIntegers(File file, int data, String firstLine) throws IOException
	{
		writeToLogs(file, firstLine + " " + String.valueOf(data));
	}
	public void writeStrings(File file, String first) throws IOException
	{
		writeToLogs(file, first);
	}

	/**
	 * Ceration of logFile
	 * @param name Name of the Log File
	 * @return File DataType
	 * @throws IOException Handling IO Errors
	 */
	public File createLogFile(String name) throws IOException
	{
		Date date = new Date();
		long epoch_time = date.getTime();
		String full_filename = name + "_" + String.valueOf(epoch_time) + ".txt";
		File file = new File("logs/" + full_filename);
		if (file.createNewFile()) {
			System.out.println("Log Created : " + full_filename);
		} else {
			System.out.println("Log File Exists : " + full_filename);
		}
		return file;
	}

	public void writeToLogs(File file, String s) throws IOException
	{
		FileWriter write = new FileWriter(file, true);
		Date dt = new Date();
		DateFormat dtf = new SimpleDateFormat("HH:mm:ss");
		write.append(dtf.format(dt)).append(" ==> ").append(s).append("\n");
		write.close();
	}

	public String sanitizeInput(String s)
	{
		s = s.trim();
		return s;
	}
	static int getMaxIndex(int[] set)
	{
		int i, max = 0, max_index = 0;
		for (i = 0;i < set.length; i++)
		{
			if (set[i] > max){
				max = set[i];
				max_index = i;
			}
		}
		return max_index;
	}
	public int getMaxValAndIndex(double[] data, boolean[] res)
	{
		int len = data.length, maxIndex = -1; double maxVal = Double.MIN_VALUE;
		for (int i = 0; i < len; i++)
		{
			if (res != null && res[i]) {continue;}
			if (data[i] > maxVal) { maxVal = data[i]; maxIndex = i;}
		}
		return maxIndex;
	}

	public String parseBooleanToString(String s, Boolean z)
	{
		if (s.equals("")) {
			s = (z)? "1": "0";
		} else {
			if (s.equals((z)? "1": "0")) {
				return s;
			} else {
				s = "?";
			}
		}
		return s;
	}
	public String parseIntegerToString(String s, int z)
	{
		if (s.equals("")) {
			s = String.valueOf(z);
		} else {
			if (s.equals(String.valueOf(z))) {
				return s;
			} else {
				s = "?";
			}
		}
		return s;
	}
}
