package Common;

import DataParser.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

public class ID3Reqs {
	private GeneralUse obj = new GeneralUse();
	public ArrayList<String[]> all_categories;
	public String[] strWorkClass = {"Private", "Self-emp-not-inc", "Self-emp-inc", "Federal-gov", "Local-gov", "State-gov", "Without-pay"
			, "Never-worked"};
	public String[] strEducation = {"Bachelors", "Some-college", "11th", "HS-grad", "Prof-school", "Assoc-acdm", "Assoc-voc",
			"9th", "7th-8th", "12th", "Masters", "1st-4th", "10th", "Doctorate", "5th-6th", "Preschool"};
	public String[] strMartial = {"Married-civ-spouse", "Divorced", "Never-married", "Separated", "Widowed",
			"Married-spouse-absent", "Married-AF-spouse"};
	public String[] strOccupation = {"Tech-support", "Craft-repair", "Other-service", "Sales", "Exec-managerial", "Prof-specialty",
			"Handlers-cleaners", "Machine-op-inspct", "Adm-clerical", "Farming-fishing", "Transport-moving", "Priv-house-serv",
			"Protective-serv", "Armed-Forces"};
	public String[] strRelationship = {"Wife", "Own-child", "Husband", "Not-in-family", "Other-relative", "Unmarried"};

	public String[] strRace = {"White", "Asian-Pac-Islander", "Amer-Indian-Eskimo", "Other", "Black"};
	public String[] strSex = {"Female", "Male"};
	public String[] strCountry = {"United-States", "Cambodia", "England", "Puerto-Rico", "Canada", "Germany", "Outlying-US(Guam-USVI-etc)", "India",
			"Japan", "Greece", "South", "China", "Cuba", "Iran", "Honduras", "Philippines", "Italy", "Poland", "Jamaica", "Vietnam", "Mexico", "Portugal",
			"Ireland", "France", "Dominican-Republic", "Laos", "Ecuador", "Taiwan", "Haiti", "Columbia", "Hungary", "Guatemala", "Nicaragua", "Scotland",
			"Thailand", "Yugoslavia", "El-Salvador", "Trinadad&Tobago", "Peru", "Hong", "Holand-Netherlands"};
	public HashMap<Integer, Integer> grpAge;
	public String[] strAge;
	public HashMap<Integer, Integer> grpfnlwgt;
	public String[] strfnlwgt;
	public HashMap<Integer, Integer> grpEduNum;
	public String[] strEduNum;
	public HashMap<Integer, Integer> grpCapGain;
	public String[] strCapGain;
	public HashMap<Integer, Integer> grpCapLoss;
	public String[] strCapLoss;
	public HashMap<Integer, Integer> grphpw;
	public String[] strhpw;
	private File logFile;

	public ID3Reqs(File output) {
		this.logFile = output;
		this.all_categories = new ArrayList<>();
	}

	/**
	 * Calculates Entropy of a given attribute in a TD Set
	 *
	 * @param req_list List of Data From Training Set to get Entropy for the attribute
	 * @return double Array : First: No. of >50 Second: no. of  <= 50 Third: Resulting Entropy
	 */
	public double[] attributeEntropy(ArrayList<AdultData> req_list) throws IOException {
		int g50 = 0, le50 = 0, size = req_list.size();
		obj.writeToLogs(this.logFile, "Received Data in Function of Size : " + size);
		for (AdultData ad : req_list) {
			if (ad.earnings == AdultData.EnumEarning.g50) {
				g50++;
			} else if (ad.earnings == AdultData.EnumEarning.le50) {
				le50++;
			}
		}
		double p1 = (size != 0) ? (g50 / (double) size) : 0;
		double p2 = (size != 0) ? (le50 / (double) size) : 0;
		obj.writeToLogs(this.logFile, "Probablity of >50K : " + p1 + " and of <=50K " + p2);
		double t1_res = (p1 != 0) ? (-p1 * (Math.log(p1) / Math.log(2))) : 0;
		double t2_res = (p2 != 0) ? p2 * (Math.log(p2) / Math.log(2)) : 0;
		double res = t1_res - t2_res;
		obj.writeToLogs(this.logFile, "Entropy of this Data =" + res);
		double final_data[] = new double[3];
		final_data[0] = g50;
		final_data[1] = le50;
		final_data[2] = res;
		return final_data;
	}

	/**
	 * Calcualtion of Information Gain Using Attribute Entropy
	 *
	 * @param req_list  List of data to go through
	 * @param group     Gets the values in the attribute/feature
	 * @param attr_name Feature Name
	 * @return The information using SHannons Formula
	 * @throws NoSuchFieldException Accessing a Restricted Variable while getting the list of attributes of a class
	 * @throws IOException          Handling I/O errors
	 */
	public double informationGain(ArrayList<AdultData> req_list, String group[], String attr_name) throws NoSuchFieldException, IOException {
		int i;
		obj.writeToLogs(this.logFile, "******************************");
		double[] entropy_of_set;
		if (req_list.size() != 0) {
			entropy_of_set = attributeEntropy(req_list);
		} else {
			return 0.0;
		}
		double total_data_Size = (entropy_of_set[0] + entropy_of_set[1]);
		double res = entropy_of_set[2];
		int group_size = group.length;
		for (i = 0; i < group_size; i++) {
			int count_freq_attr_val = 0;
			ArrayList<AdultData> newList = new ArrayList<>();
			for (AdultData ad : req_list) {
				Class<?> theClass = ad.getClass();
				Field givenField = theClass.getField(attr_name);
				try {
					if (givenField.get(ad) instanceof String) {
						String attrValue = (String) givenField.get(ad);
						if (attrValue.equals(group[i])) {
							count_freq_attr_val++;
							newList.add(ad);
						}
					}
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					try {
						obj.writeToLogs(this.logFile, e.getMessage());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}

			double partial_value = count_freq_attr_val / (total_data_Size) * attributeEntropy(newList)[2];
			obj.writeToLogs(this.logFile, "Count Data " + count_freq_attr_val + " ; Total Data " + total_data_Size);
			obj.writeToLogs(this.logFile, "Entropy of " + group[i] + " in " + attr_name + " = " + partial_value);
			res -= partial_value;
		}

		obj.writeToLogs(this.logFile, "information gain of " + attr_name + " = " + res);
		obj.writeToLogs(this.logFile, "******************************\t******************************");
		return Math.abs(res);
	}

	/**
	 * HashMap for Continuous Variable Having Data in ranges.
	 *
	 * @param req_list  List of data
	 * @param group     The continuos Variable in Ranges
	 * @param attr_name Feature
	 * @return THE IG VALUE
	 * @throws NoSuchFieldException Accessing a Restricted Variable while getting the list of attributes of a class
	 * @throws IOException          Handling I/O Errors
	 */
	public double informationGainHashMap(ArrayList<AdultData> req_list, HashMap<Integer, Integer> group, String attr_name)
			throws NoSuchFieldException, IOException {
		obj.writeToLogs(this.logFile, "******************************\t******************************");
		double[] entropy_of_set;
		if (req_list.size() != 0) {
			entropy_of_set = attributeEntropy(req_list);
		} else {
			return 0.0;
		}
		double total_data_Size = (entropy_of_set[0] + entropy_of_set[1]);
		double res = entropy_of_set[2];
		for (Integer key : group.keySet()) {
			int count_freq_attr_val = 0;
			ArrayList<AdultData> newList = new ArrayList<>();
			for (AdultData ad : req_list) {
				Class<?> theClass = ad.getClass();
				Field givenField = theClass.getField(attr_name);
				try {
					if (givenField.get(ad) instanceof Integer) {
						int attrValue = (Integer) givenField.get(ad);
						if (attrValue > key && attrValue <= group.get(key)) {
							count_freq_attr_val++;
							newList.add(ad);
						}
					}
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					try {
						obj.writeToLogs(this.logFile, e.getMessage());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}

			double partial_value = count_freq_attr_val / (total_data_Size) * attributeEntropy(newList)[2];
			obj.writeToLogs(this.logFile, "Count Data " + count_freq_attr_val + " ; Total Data " + total_data_Size);
			obj.writeToLogs(this.logFile, "Entropy of [" + key + ", " + group.get(key) + "] in " + attr_name + " = "
					+ partial_value);
			res -= partial_value;
		}

		obj.writeToLogs(this.logFile, "information gain of " + attr_name + " = " + res);
		obj.writeToLogs(this.logFile, "******************************");
		return res;
	}

	/**
	 * Fills all missing data in the array of objects
	 *
	 * @param req_list List of Data From Training Set to get Entropy for the attribute
	 * @return ArrayList Modified Data after filling Missing Attributes
	 */
	public ArrayList<AdultData> removeMissingData(ArrayList<AdultData> req_list) throws NoSuchFieldException, IllegalAccessException {
		String maxWorkClass = findHighestOccurringValue(req_list, this.strWorkClass, "workclass");
		String maxEducation = findHighestOccurringValue(req_list, this.strEducation, "education");
		String maxMartial = findHighestOccurringValue(req_list, this.strMartial, "marital_status");
		String maxOcc = findHighestOccurringValue(req_list, this.strOccupation, "occupation");
		String maxRel = findHighestOccurringValue(req_list, this.strRelationship, "relationship");
		String maxRace = findHighestOccurringValue(req_list, this.strRace, "race");
		String maxSex = findHighestOccurringValue(req_list, this.strSex, "sex");
		String maxNC = findHighestOccurringValue(req_list, this.strCountry, "native_country");
		for (AdultData ad : req_list) {
			ad.workclass = (ad.workclass.equals("?")) ? maxWorkClass : ad.workclass;
			ad.education = (ad.education.equals("?")) ? maxEducation : ad.education;
			ad.marital_status = (ad.marital_status.equals("?")) ? maxMartial : ad.marital_status;
			ad.occupation = (ad.occupation.equals("?")) ? maxOcc : ad.occupation;
			ad.relationship = (ad.relationship.equals("?")) ? maxRel : ad.relationship;
			ad.race = (ad.race.equals("?")) ? maxRace : ad.race;
			ad.sex = (ad.sex.equals("?")) ? maxSex : ad.sex;
			ad.native_country = (ad.native_country.equals("?")) ? maxNC : ad.native_country;
		}
		return req_list;
	}

	public ID3Category getGroupsForData(ArrayList<AdultData> list_of_data, String oType) throws NoSuchFieldException, IllegalAccessException {
		HashMap<Integer, Integer> SetOfValues = new HashMap<>();
		ID3Category category = new ID3Category();
		int i;
		Set<Integer> uniqueValues = new HashSet<>();
		for (AdultData ad : list_of_data) {
			Class<?> getClass = ad.getClass();
			Field givenField = getClass.getField(oType);
			int val = (Integer) givenField.get(ad);
			uniqueValues.add(val);
		}
		TreeSet<Integer> sortedUniqueValues = new TreeSet<>(uniqueValues);
		int smallestData = sortedUniqueValues.first();
		int greatestData = sortedUniqueValues.last();
		int param = (int) Math.round(0.5 * (greatestData - smallestData)); // Assumption for levelling of data
		for (i = smallestData; i < greatestData; i += param) {
			if (!SetOfValues.containsKey(i)) {
				SetOfValues.put(i, i + param);
			}
		}
		try {
			obj.writeToLogs(this.logFile, String.format("%d groups were formed for continuous data of %s ranging % d to %d", SetOfValues.size(),
					oType, smallestData, greatestData));
			for (Integer key : SetOfValues.keySet()) {
				obj.writeToLogs(this.logFile, key + "\t" + SetOfValues.get(key));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.printf("%d groups were formed for continuous data of %s ranging % d to %d\n", SetOfValues.size(),
				oType, smallestData, greatestData);
		String[] setOfVals = new String[SetOfValues.size()];
		int k = 0;
		for (Integer key : SetOfValues.keySet()) {
			setOfVals[k] = String.valueOf(key) + "," + String.valueOf(SetOfValues.get(key));
			k++;
		}
		category.first = SetOfValues;
		category.second = setOfVals;
		return category;
	}

	/**
	 * This is for Grouping Data of COntinuous Variables
	 *
	 * @param req_list  The list of data
	 * @param list      Get the List of Attributes
	 * @param attr_name The feature using which twe get the object attribute to search for
	 * @return the String which occurs with highest Frequency
	 * @throws NoSuchFieldException   Handler for NoField While using Reflect class in Java
	 * @throws IllegalAccessException Accessing a Private Variable while getting the list of attributes of a class
	 */
	private String findHighestOccurringValue(ArrayList<AdultData> req_list, String[] list, String attr_name)
			throws NoSuchFieldException, IllegalAccessException {
		int countFrequency[] = new int[list.length], i;
		for (i = 0; i < list.length; i++) {
			countFrequency[i] = 0;
		}
		for (i = 0; i < list.length; i++) {
			for (AdultData ad : req_list) {
				Class<?> theClass = ad.getClass();
				Field givenField = theClass.getField(attr_name);
				String val = (String) givenField.get(ad);
				if (val.equals(list[i])) {
					countFrequency[i]++;
				}
			}
		}
		int maxFrequencyIndex = GeneralUse.getMaxIndex(countFrequency);
		return list[maxFrequencyIndex];
	}

	/**
	 * Write Input Data to a new FIle so that I dont have to categorize data every time
	 *
	 * @param filename     The name of file to write to
	 * @param completeList The list of Data after Categorizing
	 * @return If the process is completed or not
	 * @throws IOException Handling I/O errors
	 */
	public boolean writeToANewFile(File filename, ArrayList<AdultData> completeList) throws IOException {
		if (filename.exists()) {
			return false;
		} else {
			FileWriter fw = new FileWriter(filename.getAbsolutePath());
			for (AdultData ad : completeList) {
				fw.write(ad.age + "," + ad.workclass + "," + ad.fnlwgt + "," + ad.education + "," + ad.education_num + "," + ad.marital_status
						+ "," + ad.occupation + "," + ad.relationship + "," + ad.race + "," + ad.sex + "," + ad.capital_gain + "," + ad.capital_loss + "," +
						ad.hours_per_week + "," + ad.native_country + "," + ad.earnings.name() + "\n");
			}
			fw.close();
		}
		return true;
	}

	/**
	 * Parse Numbers
	 *
	 * @param data String Data
	 * @return Returns Integer
	 * @throws IOException Handling IO Errors
	 */
	private int parseNumericType(String data) throws IOException {
		int val;
		data = obj.sanitizeInput(data);
		try {
			val = Integer.parseInt(data);
		} catch (Exception e) {
			obj.writeStrings(logFile, "ERROR: " + e.getMessage());
			return -1;
		}
		return val;
	}

	/**
	 * @param each_line Line
	 * @return Dividing data to attributes
	 * @throws IOException Handling IO Errors
	 */
	public AdultData partitionData(String[] each_line) throws IOException {
		AdultData ad = new AdultData();
		ad.age = parseNumericType(each_line[0]);
		ad.workclass = obj.sanitizeInput(each_line[1]);
		ad.fnlwgt = parseNumericType(each_line[2]);
		ad.education = obj.sanitizeInput(each_line[3]);
		ad.education_num = parseNumericType(each_line[4]);
		ad.marital_status = obj.sanitizeInput(each_line[5]);
		ad.occupation = obj.sanitizeInput(each_line[6]);
		ad.relationship = obj.sanitizeInput(each_line[7]);
		ad.race = obj.sanitizeInput(each_line[8]);
		ad.sex = obj.sanitizeInput(each_line[9]);
		ad.capital_gain = parseNumericType(each_line[10]);
		ad.capital_loss = parseNumericType(each_line[11]);
		ad.hours_per_week = parseNumericType(each_line[12]);
		ad.native_country = obj.sanitizeInput(each_line[13]);
		try {
			each_line[14] = obj.sanitizeInput(each_line[14]);
			ad.earnings = ad.parseEnumEarning(each_line[14]);
		} catch (Exception e) {
			obj.writeStrings(logFile, "ERROR: " + e.getMessage());
		}
		return ad;
	}
}

