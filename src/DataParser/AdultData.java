package DataParser;

public class AdultData {
	public enum EnumEarning {g50, le50}

	public int age;
	public String workclass;
	public int fnlwgt;
	public String education;
	public int education_num;
	public String marital_status;
	public String occupation;
	public String relationship;
	public String race;
	public String sex;
	public int capital_gain, capital_loss, hours_per_week;
	public String native_country;
	public EnumEarning earnings;

	/**
	 * Parser for Enumeration Type
	 * @param s String to convert to enum
	 * @return EnumEarning Type Data
	 */
	public EnumEarning parseEnumEarning(String s)
	{
		switch (s) {
			case "<=50K":
				return EnumEarning.le50;
			case ">50K":
				return EnumEarning.g50;
			case "<=50K.":
				return EnumEarning.le50;
			case ">50K.":
				return EnumEarning.g50;
			case "le50":
				return EnumEarning.le50;
			case "g50":
				return EnumEarning.g50;
			default:
				return EnumEarning.g50;
		}
	}
}
