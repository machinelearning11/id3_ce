package DataParser;

public class CountOfZooData
{
	final public int COUNT_HAIR = 2;
	final public int COUNT_FEATHERS = 2;
	final public int COUNT_EGGS = 2;
	final public int COUNT_MILK = 2;
	final public int COUNT_AIRBORNE = 2;
	final public int COUNT_AQUATIC = 2;
	final public int COUNT_PREDATOR = 2;
	final public int COUNT_TOOTHED = 2;
	final public int COUNT_BACKBONE = 2;
	final public int COUNT_BREATHES = 2;
	final public int COUNT_VENOMOUS = 2;
	final public int COUNT_FINS = 2;
	final public int COUNT_LEGS = 6;
	final public int COUNT_TAIL = 2;
	final public int COUNT_DOMESTIC = 2;
	final public int COUNT_CATSIZE = 2;
	final public int[] type_legs = {0,2,4,5,6,8};
}