package DataParser;

public class ZooData {
	/**
	 * List of attributes for a Zoo Object
	 */
	public String name;
	public boolean hair;
	public boolean feathers;
	public boolean eggs;
	public boolean milk;
	public boolean airborne;
	public boolean aquatic;
	public boolean predator;
	public boolean toothed;
	public boolean backbone;
	public boolean breathes;
	public boolean venomous;
	public boolean fins;
	public int legs;
	public boolean tail;
	public boolean domestic;
	public boolean catsize;
	public int type;
}