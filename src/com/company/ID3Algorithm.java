package com.company;
import Common.*;
import DataParser.AdultData;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Scanner;

class ID3Algorithm implements Algorithms{

	private File inputFile;
	private GeneralUse commonObj;
	private File outputLogFile;
	private ArrayList<AdultData> trainingData;
	// private ArrayList<AdultData> trainingDataForPruning;
	private ArrayList<AdultData> testDataSet;
	private File testDataFile;
	private ID3Reqs algoReqs;
	private String[] attributes_arr = {"age", "workclass", "fnlwgt", "education", "education_num", "marital_status", "occupation"
	, "relationship", "race", "sex", "capital_gain", "capital_loss", "hours_per_week", "native_country"};
	ID3Algorithm(String filename, String testData)
	{
		this.inputFile = new File(filename);
		this.commonObj = new GeneralUse();
		this.trainingData = new ArrayList<>();
		this.testDataFile = new File(testData);
		this.testDataSet = new ArrayList<>();
	}

	/**
	 * This function parses the input file to the AdultData Class Objects
	 * @throws IOException Handling IO Errors
	 */
	public void ParseFile() throws IOException
	{
		String line;
		String[] each_line;
		Scanner sc = new Scanner(inputFile);
		while (sc.hasNextLine())
		{
			AdultData ad;
			line = sc.nextLine();
			each_line = line.split(",");
			if (each_line.length == 15) {
				ad = algoReqs.partitionData(each_line);
				trainingData.add(ad);
			}
			else {
				try {
					throw new MissingAttributeException("The Data contains some missing Attributes");
				} catch (MissingAttributeException e) {
					commonObj.writeStrings(outputLogFile, e.getMessage() + " in Data Line: " + line);
				}
			}
		}

	}

	/**
	 * This functions parses the output file
	 * TODO: Merge both file parsing options into a single function
	 * @throws IOException Handling IO Errors
	 */
	private void ParseOutputFile() throws IOException
	{
		String line;
		String[] each_line;
		Scanner sc = new Scanner(this.testDataFile);
		while (sc.hasNextLine())
		{
			AdultData bad;
			line = sc.nextLine();
			each_line = line.split(",");
			try {
				if (each_line.length == 15) {
					bad = algoReqs.partitionData(each_line);
					this.testDataSet.add(bad);
				}
			}
			catch (Exception e)
			{
				commonObj.writeStrings(outputLogFile, e.getMessage() + " in Data Line: " + line);
			}
		}

	}

	/**
	 * This function adds all categories to a single arraylist. Could have been made better than this
	 * @param reqs The required categories to insert
	 * @return Object for referencing this ArrayList
	 */
	private static ID3Reqs addAllCategories(ID3Reqs reqs)
	{
		reqs.all_categories.add(reqs.strAge);
		reqs.all_categories.add(reqs.strWorkClass);
		reqs.all_categories.add(reqs.strfnlwgt);
		reqs.all_categories.add(reqs.strEducation);
		reqs.all_categories.add(reqs.strEduNum);
		reqs.all_categories.add(reqs.strMartial);
		reqs.all_categories.add(reqs.strOccupation);
		reqs.all_categories.add(reqs.strRelationship);
		reqs.all_categories.add(reqs.strRace);
		reqs.all_categories.add(reqs.strSex);
		reqs.all_categories.add(reqs.strCapGain);
		reqs.all_categories.add(reqs.strCapLoss);
		reqs.all_categories.add(reqs.strhpw);
		reqs.all_categories.add(reqs.strCountry);
		return reqs;
	}

	/**
	 * Initiate Implemetation
	 * @throws IOException Handling I/O errors
	 * @throws NoSuchFieldException Handler for NoField While using Reflect class in Java
	 * @throws IllegalAccessException Accessing a Private Variable while getting the list of attributes of a class
	 */
	public String startImplementation() throws IOException, NoSuchFieldException, IllegalAccessException {
		this.outputLogFile = commonObj.createLogFile("id3Implementation");
		this.algoReqs = new ID3Reqs(outputLogFile);
		commonObj.writeStrings(outputLogFile, "Started Parsing File at " + inputFile.getAbsolutePath());
		ParseFile();
		ParseOutputFile();
		commonObj.writeStrings(outputLogFile, "Completed Parsing File");
		commonObj.writeStrings(outputLogFile, "Total instances in data " + this.trainingData.size());

		/*Missing Data Fixes Are Done Below*/
		ArrayList<AdultData> new_data = algoReqs.removeMissingData(this.trainingData);
		ArrayList<AdultData> new_test_data = algoReqs.removeMissingData(this.testDataSet);
		File f = new File("data/adult_mod.txt"); File outNew = new File("tests/adult_testDat_mod.txt");
		boolean res = algoReqs.writeToANewFile(f, new_data);
		boolean oures = algoReqs.writeToANewFile(outNew, new_test_data);

		if (oures) {
			commonObj.writeStrings(outputLogFile, "New Output File has now been Parsed");
			this.testDataFile = outNew;
		} else {
			commonObj.writeStrings(outputLogFile, "New Output File File Exists. No modifications " +
					"done on Testing Data");
		}
		if (res){ commonObj.writeStrings(outputLogFile, "New File Does Not Exists. So now Missing Values have been filled");}
		else {commonObj.writeStrings(outputLogFile, "New File Exists. No modifications done on Training Data");}
		/* Grouping Continuous Data */
		algoReqs.grpAge = algoReqs.getGroupsForData(this.trainingData, "age").first;
		algoReqs.strAge = algoReqs.getGroupsForData(this.trainingData, "age").second;
		algoReqs.grpfnlwgt = algoReqs.getGroupsForData(this.trainingData, "fnlwgt").first;
		algoReqs.strfnlwgt = algoReqs.getGroupsForData(this.trainingData, "fnlwgt").second;
		algoReqs.grpEduNum = algoReqs.getGroupsForData(this.trainingData, "education_num").first;
		algoReqs.strEduNum = algoReqs.getGroupsForData(this.trainingData, "education_num").second;
		algoReqs.grpCapGain = algoReqs.getGroupsForData(this.trainingData, "capital_gain").first;
		algoReqs.strCapGain = algoReqs.getGroupsForData(this.trainingData, "capital_gain").second;
		algoReqs.grpCapLoss = algoReqs.getGroupsForData(this.trainingData, "capital_loss").first;
		algoReqs.strCapLoss = algoReqs.getGroupsForData(this.trainingData, "capital_loss").second;
		algoReqs.grphpw = algoReqs.getGroupsForData(this.trainingData, "hours_per_week").first;
		algoReqs.strhpw = algoReqs.getGroupsForData(this.trainingData, "hours_per_week").second;
		algoReqs = addAllCategories(algoReqs);
		/* Start Id3 Implementation */
		this.trainingData = new_data;
		double[] set_entropy = algoReqs.attributeEntropy(this.trainingData);
		commonObj.writeStrings(outputLogFile, "Total entropy in data " + set_entropy[0] + " +ves " +
				set_entropy[1] + " -ves and the entropy is " + set_entropy[2]);
		// Get Root Node
		double ig_gains[] = new double[14];
		ig_gains = getInfoGainsOfAllAttrs(ig_gains, algoReqs, this.trainingData);
		int maxEntropyIndex = this.commonObj.getMaxValAndIndex(ig_gains, null);
		System.out.println("The root is " + attributes_arr[maxEntropyIndex]);
		Id3TreeNode root = new Id3TreeNode(this.trainingData, maxEntropyIndex, null,
				algoReqs.all_categories.get(maxEntropyIndex), null);
		makeTree(root);
		System.out.println("Id3 Tree Building Complete :)\n");
		int treeDepth = Id3TreeNode.treeDepth(root);
		System.out.println("Depth of the tree formed is " + treeDepth);
		commonObj.writeStrings(outputLogFile, "Depth of the tree formed is " + treeDepth);
		// Testing Accuracy with test data
		double accuracy = computeAccuracy(this.testDataFile, root);
		System.out.println("Accuracy of this test is " + accuracy);
		return this.outputLogFile.getName();
	}

	/**
	 * Makes the tree Obviously!!
	 * @param node The parent node for the tree for building the child recursing
	 * @throws IllegalAccessException Accessing a Private Variable while getting the list of attributes of a class
	 */
	private void makeTree(Id3TreeNode node) throws IllegalAccessException
	{
		if (node.isLeaf) {return;}
		node.useStatusOfAttribute[node.id] = true;
		for (String attrVal: node.Group)
		{
			/*ArrayList<String> req_params = new ArrayList<>();
			req_params.add(attributes_arr[node.id]);*/
			Id3TreeNode new_node;
			ArrayList<AdultData> new_data = getDataByParameter(node.data, attributes_arr[node.id], attrVal);
			ID3Reqs getInfoGain = new ID3Reqs(this.outputLogFile);
			if (new_data.size() == 0) {
				continue;
			}
			double[] infoGainsList = new double[14];
			try {
				infoGainsList = getInfoGainsOfAllAttrs(infoGainsList, getInfoGain, new_data);
				int maxEntropyIndex = this.commonObj.getMaxValAndIndex(infoGainsList, node.useStatusOfAttribute);
				if (maxEntropyIndex != -1) {
					new_node = new Id3TreeNode(new_data, maxEntropyIndex, attrVal,
							algoReqs.all_categories.get(maxEntropyIndex), node);
				}
				else {
					new_node = new Id3TreeNode(new_data, maxEntropyIndex, attrVal,
							null, node);
					new_node.isLeaf = true;
					new_node.leafValue = new_data.get(0).earnings;
				}
				node.next.add(new_node);
				makeTree(new_node);
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
				try {
					commonObj.writeToLogs(this.outputLogFile, e.getMessage());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	private ArrayList<AdultData> getDataByParameter(ArrayList<AdultData> data, String param, String value) throws IllegalAccessException
	{
		ArrayList<AdultData> new_data = new ArrayList<>();
		for (AdultData ad: data)
		{
			Class<?> theClass = ad.getClass();
			try {
				Field givenField = theClass.getField(param);
				if (givenField.get(ad) instanceof Integer)
				{
					int attrValInTD = (Integer) givenField.get(ad);
					String[] splitedGroup = value.split(","); // This is the attribute group value
					int low = Integer.parseInt(splitedGroup[0]);
					int high = Integer.parseInt(splitedGroup[1]);
					if (attrValInTD >= low && attrValInTD < high)
					{
						new_data.add(ad);
					}
				} else {
					String attrValinTD = (String) givenField.get(ad); // This is the attribute group value
					if (attrValinTD.equals(value)) // Comparing with the value in TD
					{
						new_data.add(ad);
					}
				}
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
				try {
					commonObj.writeToLogs(this.outputLogFile, e.getMessage());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		return new_data;
	}
	private double[] getInfoGainsOfAllAttrs(double[] ig_gains, ID3Reqs infoGainReqs, ArrayList<AdultData> data) throws NoSuchFieldException
	{
		try {
			ig_gains[0] = infoGainReqs.informationGainHashMap(data, this.algoReqs.grpAge, attributes_arr[0]);
			ig_gains[1] = infoGainReqs.informationGain(data, this.algoReqs.strWorkClass, attributes_arr[1]);
			ig_gains[2] = infoGainReqs.informationGainHashMap(data, this.algoReqs.grpfnlwgt, attributes_arr[2]);
			ig_gains[3] = infoGainReqs.informationGain(data, this.algoReqs.strEducation, attributes_arr[3]);
			ig_gains[4] = infoGainReqs.informationGainHashMap(data, this.algoReqs.grpEduNum, attributes_arr[4]);
			ig_gains[5] = infoGainReqs.informationGain(data, this.algoReqs.strMartial, attributes_arr[5]);
			ig_gains[6] = infoGainReqs.informationGain(data, this.algoReqs.strOccupation, attributes_arr[6]);
			ig_gains[7] = infoGainReqs.informationGain(data, this.algoReqs.strRelationship, attributes_arr[7]);
			ig_gains[8] = infoGainReqs.informationGain(data, this.algoReqs.strRace, attributes_arr[8]);
			ig_gains[9] = infoGainReqs.informationGain(data, this.algoReqs.strSex, attributes_arr[9]);
			ig_gains[10] = infoGainReqs.informationGainHashMap(data, this.algoReqs.grpCapGain, attributes_arr[10]);
			ig_gains[11] = infoGainReqs.informationGainHashMap(data, this.algoReqs.grpCapLoss, attributes_arr[11]);
			ig_gains[12] = infoGainReqs.informationGainHashMap(data, this.algoReqs.grphpw, attributes_arr[12]);
			ig_gains[13] = infoGainReqs.informationGain(data, this.algoReqs.strCountry, attributes_arr[13]);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			try {
				commonObj.writeToLogs(this.outputLogFile, e.getMessage());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return ig_gains;
	}

	/**
	 * COmpute Accuracy of the test data
	 * @param f The test file to read data from
	 * @param root The Tree's root Node
	 * @return the accuracy of my tree
	 * @throws IOException Handling I/O errors
	 */
	private double computeAccuracy(File f, Id3TreeNode root) throws IOException
	{
		double accuracy, total = 0, correct = 0;
		String line;
		String each_line[];
		boolean result;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine())
			{
				line = sc.nextLine();
				commonObj.writeToLogs(this.outputLogFile, line);
				each_line = line.split(",");
				result = traverseTree(root, each_line);
				if (result)
				{
					correct++;
				}
				total++;
			}

		} catch (FileNotFoundException e) {
			this.commonObj.writeToLogs(this.outputLogFile, e.getMessage());
			e.printStackTrace();
		}
		accuracy = correct/ total;
		this.commonObj.writeToLogs(this.outputLogFile, String.format("Out of %.3f test data, the correct outputs is " +
				"obtained in %.3f cases with accuracy of %.3f", total, correct, accuracy));
		return accuracy;
	}

	/**
	 *
	 * @param node The root node
	 * @param line The test line for testing the attribute
	 * @return TRUE/FALSE if match is found
	 * @throws IOException Handling I/O errors
	 */
	private boolean traverseTree(Id3TreeNode node, String[] line) throws IOException
	{
		boolean res = false;
		int id = node.id;
		AdultData ad = new AdultData();
		if (id == -1)
		{
			AdultData.EnumEarning answer = ad.parseEnumEarning(line[14]);
			commonObj.writeToLogs(outputLogFile, "So the value in this test is data is " + answer.name() + " " +
					"and the my algo gives " + node.leafValue.name());
			if (node.leafValue == answer)
			{
				res = true;
			}
		}
		else {
			String test_value = line[id];
			if (id == 0 || id == 2 || id == 4 || id == 10 || id == 11 || id == 12)
			{
				int val = Integer.parseInt(test_value);
				int size = node.next.size();
				for (int i = 0;i < size; i++)
				{
					int lower = Integer.parseInt(node.next.get(i).req_param.split(",")[0]);
					int higher = Integer.parseInt(node.next.get(i).req_param.split(",")[1]);
					if (val >= lower && val < higher)
					{
						commonObj.writeToLogs(outputLogFile, "Found at index " + i + " in " + attributes_arr[id]);
						res = traverseTree(node.next.get(i), line);
						break;
					}
				}
			}
			else {
				int size = node.next.size();
				for (int i = 0;i < size; i++)
				{
					if (test_value.equals(node.next.get(i).req_param))
					{
						commonObj.writeToLogs(outputLogFile, "Found at index " + i + " in " + attributes_arr[id]);
						res = traverseTree(node.next.get(i), line);
						break;
					}
				}
			}
		}
		return res;
	}
}
