package com.company;

import java.io.IOException;

public interface Algorithms {
	void ParseFile() throws IOException;
	String startImplementation() throws IOException, NoSuchFieldException, IllegalAccessException;
}
