package com.company;
import DataParser.AdultData;
import java.util.ArrayList;
import java.util.Arrays;

class Id3TreeNode implements Cloneable{
	int id;
	ArrayList<AdultData> data;
	String req_param;
	String[] Group;
	Id3TreeNode prev;
	ArrayList<Id3TreeNode> next = new ArrayList<>();
	boolean isLeaf = false;
	boolean[] useStatusOfAttribute;
	AdultData.EnumEarning leafValue = null;
	Id3TreeNode(ArrayList<AdultData> data, int id, String req_param, String[] strGrp,
				Id3TreeNode prev)
	{
		this.prev = prev;
		this.data = data;
		this.id = id;
		this.req_param = req_param;
		this.Group = strGrp;
		if (this.prev != null && id != -1)
		{
			this.useStatusOfAttribute = this.prev.useStatusOfAttribute.clone();
			this.useStatusOfAttribute[id] = true;
		}
		else {
			this.useStatusOfAttribute = new boolean[14];
			Arrays.fill(this.useStatusOfAttribute, false);
		}
	}

	/**
	 * Returns Depth of the tree
	 * @param root Root Node
	 * @return Depth/ Inetger Type
	 */
	static int treeDepth(Id3TreeNode root)
	{
		if (root == null) {
			return 0;
		}
		int depth = 0;
		for (Id3TreeNode node: root.next)
		{
			depth = Math.max(depth, treeDepth(node));
		}
		return depth + 1;
	}
}