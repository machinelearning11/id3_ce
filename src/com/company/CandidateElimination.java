package com.company;
import DataParser.*;
import Common.GeneralUse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

class CandidateElimination implements Algorithms{
	private ArrayList<ZooData> animalList;
	private File fileName;
	private GeneralUse commonImplementor;
	private File outputLogFile;

	CandidateElimination(String filename) throws FileNotFoundException {
		this.animalList = new ArrayList<>();
		this.fileName = new File(filename);
		this.commonImplementor = new GeneralUse();
	}

	/**
	 * This functions parses the input into Attributes of ZooData
	 * @throws IOException For Handling File Errors
	 */
	public void ParseFile() throws IOException
	{
		String line;
		String[] csValues;
		Scanner sc = new Scanner(this.fileName);
		ZooData z;
		commonImplementor.writeStrings(outputLogFile, "Started Parsing File at " + fileName.getAbsolutePath());
		while (sc.hasNextLine())
		{
			z = new ZooData();
			line = sc.nextLine();
			csValues = line.split(",");
			if (csValues.length == 18) {
				z.name = csValues[0];
				z.hair = Integer.parseInt(csValues[1]) == 1;
				z.feathers = Integer.parseInt(csValues[2]) == 1;
				z.eggs = Integer.parseInt(csValues[3]) == 1;
				z.milk = Integer.parseInt(csValues[4]) == 1;
				z.airborne = Integer.parseInt(csValues[5]) == 1;
				z.aquatic = Integer.parseInt(csValues[6]) == 1;
				z.predator = Integer.parseInt(csValues[7]) == 1;
				z.toothed = Integer.parseInt(csValues[8]) == 1;
				z.backbone = Integer.parseInt(csValues[9]) == 1;
				z.breathes = Integer.parseInt(csValues[10]) == 1;
				z.venomous = Integer.parseInt(csValues[11]) == 1;
				z.fins = Integer.parseInt(csValues[12]) == 1;
				z.legs = Integer.parseInt(csValues[13]);
				z.tail = Integer.parseInt(csValues[14]) == 1;
				z.domestic = Integer.parseInt(csValues[15]) == 1;
				z.catsize = Integer.parseInt(csValues[16]) == 1;
				z.type = Integer.parseInt(csValues[17]);
				animalList.add(z);
			}
			else {
				try {
					throw new MissingAttributeException("The Data contains some missing Attributes");
				} catch (MissingAttributeException e) {
					commonImplementor.writeStrings(outputLogFile, e.getMessage());
				}
			}
		}
	}

	/**
	 * Start Implementation for Candidate Elimination
	 * @throws IOException For Handling File Errors
	 */
	public String startImplementation() throws IOException
	{
		outputLogFile = commonImplementor.createLogFile("candidateElimination");
		ParseFile();
		commonImplementor.writeToLogs(outputLogFile,
				"No. of Training Data instances Found " + animalList.size());

		// Get all required data to create Specific Region
		commonImplementor.writeStrings(outputLogFile, "Creating Specific Boundary for the attribute Type");
		ArrayList<ZooData> typeOne, typeTwo, typeThree, typeFour, typeFive, typeSix, typeSeven;
		typeOne = commonImplementor.selectIntDataByParam(animalList, "type", 1);
		commonImplementor.writeSimpleIntegers(outputLogFile, typeOne.size(), "No. of instances in type 1 ");
		typeTwo = commonImplementor.selectIntDataByParam(animalList, "type", 2);
		commonImplementor.writeSimpleIntegers(outputLogFile, typeTwo.size(), "No. of instances in type 2 ");
		typeThree = commonImplementor.selectIntDataByParam(animalList, "type", 3);
		commonImplementor.writeSimpleIntegers(outputLogFile, typeThree.size(), "No. of instances in type 3 ");
		typeFour = commonImplementor.selectIntDataByParam(animalList, "type", 4);
		commonImplementor.writeSimpleIntegers(outputLogFile, typeFour.size(), "No. of instances in type 4 ");
		typeFive = commonImplementor.selectIntDataByParam(animalList, "type", 5);
		commonImplementor.writeSimpleIntegers(outputLogFile, typeFive.size(), "No. of instances in type 5 ");
		typeSix = commonImplementor.selectIntDataByParam(animalList, "type", 6);
		commonImplementor.writeSimpleIntegers(outputLogFile, typeSix.size(), "No. of instances in type 6 ");
		typeSeven = commonImplementor.selectIntDataByParam(animalList, "type", 7);
		commonImplementor.writeSimpleIntegers(outputLogFile, typeSeven.size(), "No. of instances in type 7 ");

		// Get the Specific Boundary Corresponding to each type Find-S
		// commonImplementor.writeStrings(this.outputLogFile, "Find-S ALgorithm For Calculating Specific Boundary");
		// calculateAllSpecificBoundaries(typeOne, typeTwo, typeThree, typeFour, typeFour, typeFive, typeSix);
		// Candidate Elimination
		commonImplementor.writeStrings(this.outputLogFile, "Calculating General and Specific Using Version Space. " +
				"Real Begginning For Candidate Elimination");
		calcGenSpec(animalList, 1);
		calcGenSpec(animalList, 2);
		calcGenSpec(animalList, 3);
		calcGenSpec(animalList, 4);
		calcGenSpec(animalList, 5);
		calcGenSpec(animalList, 6);
		calcGenSpec(animalList, 7);
		return this.outputLogFile.getName();
	}

	/**
	 * Calculates General and Specific Boundary
	 * @param allData ZooData ArrayList
	 * @param type Integers 1-7
	 * @throws IOException For Handling File Errors
	 */
	private void calcGenSpec(ArrayList<ZooData> allData, int type) throws IOException
	{
		String[] sp = new String[16];
		ZooData each;
		Arrays.fill(sp, "");
		ArrayList<String[]> gp = new ArrayList<>();
		int i, j, len = allData.size();
		for (i = 0; i < len; i++)
			if (allData.get(i).type == type) { break; }
		sp = parseSpecificRegion(sp, allData.get(i));
		for (int k = 0; k < 16; k++)
		{
			if (!sp[k].equals("?")) {
				String[] hp = new String[16];
				Arrays.fill(hp, "?");
				hp[k] = sp[k];
				gp.add(hp);
			}
		}
		for (j = i + 1; j < len; j++)
		{
			each = allData.get(j);
			if (each.type == type)
			{
				sp = parseSpecificRegion(sp, each);
				gp = parseGenericRegionForPositives(gp, sp);
			} else {
				gp = parseGenericRegionforNegatives(gp, each, sp);
			}
			if (gp.size() == 1) {break;}
		}
		commonImplementor.writeSimpleArrayOfStrings(this.outputLogFile,sp, 16,
				"Specific Boundary for type " + type);
		commonImplementor.writeSimpleArrayListOfStrings(this.outputLogFile, gp, gp.size(), "Generic Boundary for type " + type);

	}

	/**
	 * Parsing Data For Specific Region. Too big to handle :P
	 * @param sp Specific Region Boundary List
	 * @param z Each Data
	 * @return The Calculated Specific Region
	 */
	private String[] parseSpecificRegion(String[] sp, ZooData z)
	{
		int i;
		for (i = 0; i < 16; i++)
		{
			switch (i)
			{
				case 0:
					sp[0] = commonImplementor.parseBooleanToString(sp[0], z.hair);
					break;
				case 1:
					sp[1] = commonImplementor.parseBooleanToString(sp[1], z.feathers);
					break;
				case 2:
					sp[2] = commonImplementor.parseBooleanToString(sp[2], z.eggs);
					break;
				case 3:
					sp[3] = commonImplementor.parseBooleanToString(sp[3], z.milk);
					break;
				case 4:
					sp[4] = commonImplementor.parseBooleanToString(sp[4], z.airborne);
					break;
				case 5:
					sp[5] = commonImplementor.parseBooleanToString(sp[5], z.aquatic);
					break;
				case 6:
					sp[6] = commonImplementor.parseBooleanToString(sp[6], z.predator);
					break;
				case 7:
					sp[7] = commonImplementor.parseBooleanToString(sp[7], z.toothed);
					break;
				case 8:
					sp[8] = commonImplementor.parseBooleanToString(sp[8], z.backbone);
					break;
				case 9:
					sp[9] = commonImplementor.parseBooleanToString(sp[9], z.breathes);
					break;
				case 10:
					sp[10] = commonImplementor.parseBooleanToString(sp[10], z.venomous);
					break;
				case 11:
					sp[11] = commonImplementor.parseBooleanToString(sp[11], z.fins);
					break;
				case 12:
					sp[12] = commonImplementor.parseIntegerToString(sp[12], z.legs);
					break;
				case 13:
					sp[13] = commonImplementor.parseBooleanToString(sp[13], z.tail);
					break;
				case 14:
					sp[14] = commonImplementor.parseBooleanToString(sp[14], z.domestic);
					break;
				case 15:
					sp[15] = commonImplementor.parseBooleanToString(sp[15], z.catsize);
					break;
			}
		}
		return sp;
	}

	/**
	 * Get Genric Bundary Space
	 * @param gp The Arraylist for storing this
	 * @param sp The Currnt Specific BOundary
	 * @return The Modified ArrayList
	 */
	private ArrayList<String[]> parseGenericRegionForPositives(ArrayList<String[]> gp, String[] sp)
	{
		int i, j, len = gp.size();
		ArrayList<String[]> new_gp = new ArrayList<>();
		for (i = 0; i < len; i++)
		{
			for (j = 0; j < 16; j++)
			{
				if (!gp.get(i)[j].equals("?")) {
					boolean check = gp.get(i)[j].equals(sp[j]);
					if (!check) {
						break;
					}
				}
			}
			if (j == 16) {
				new_gp.add(gp.get(i));
			}
		}
		if (new_gp.size() == 0) {
			return gp;
		}
		return new_gp;
	}

	/**
	 * This is a diffeernt Function for Handling Negative Results
	 * @param gp The Arraylist for storing this
	 * @param z The Currnt Data
	 * @param sp The Specific Boundary calculated Till Now
	 * @return ArrayList of General Boundary
	 */
	private ArrayList<String[]> parseGenericRegionforNegatives(ArrayList<String[]> gp, ZooData z, String[] sp)
	{
		int i, len = gp.size(), garbage = -1;
		for (i = 0; i < 16; i++)
		{
			switch (i) {
				case 0:
					if ((!z.hair && sp[0].equals("1")) || (z.hair && sp[0].equals("0"))) {
						garbage = i;
					}
					break;
				case 1:
					if ((!z.feathers && sp[1].equals("1")) || (z.feathers && sp[1].equals("0"))) {
						garbage = i;
					}
					break;
				case 2:
					if ((!z.eggs && sp[2].equals("1")) || (z.eggs && sp[2].equals("0"))) {
						garbage = i;
					}
					break;
				case 3:
					if ((!z.milk && sp[3].equals("1")) || (z.milk && sp[3].equals("0"))) {
						garbage = i;
					}
					break;
				case 4:
					if ((!z.airborne && sp[4].equals("1")) || (z.airborne && sp[4].equals("0"))) {
						garbage = i;
					}
					break;
				case 5:
					if ((!z.aquatic && sp[5].equals("1")) || (z.aquatic && sp[5].equals("0"))) {
						garbage = i;
					}
					break;
				case 6:
					if ((!z.predator && sp[6].equals("1")) || (z.predator && sp[6].equals("0"))) {
						garbage = i;
					}
					break;
				case 7:
					if ((!z.toothed && sp[7].equals("1")) || (z.toothed && sp[7].equals("0"))) {
						garbage = i;
					}
					break;
				case 8:
					if ((!z.backbone && sp[8].equals("1")) || (z.backbone && sp[8].equals("0"))) {
						garbage = i;
					}
					break;
				case 9:
					if ((!z.breathes && sp[9].equals("1")) || (z.breathes && sp[9].equals("0"))) {
						garbage = i;
					}
					break;
				case 10:
					if ((!z.venomous && sp[10].equals("1")) || (z.venomous && sp[10].equals("0"))) {
						garbage = i;
					}
					break;
				case 11:
					if ((!z.fins && sp[11].equals("1")) || (z.fins && sp[11].equals("0"))) {
						garbage = i;
					}
					break;
				case 12:
					if (!(sp[12].equals("?"))) {
						if (Integer.parseInt(sp[12]) != z.legs)
						{
							garbage = i;
						}
					}
					break;
				case 13:
					if ((!z.tail && sp[13].equals("1")) || (z.tail && sp[13].equals("0"))) {
						garbage = i;
					}
					break;
				case 14:
					if ((!z.domestic && sp[14].equals("1")) || (z.domestic && sp[14].equals("0"))) {
						garbage = i;
					}
					break;
				case 15:
					if ((!z.catsize && sp[15].equals("1")) || (z.catsize && sp[15].equals("0"))) {
						garbage = i;
					}
					break;
			}
		}
		for (i = 0; i < len; i++)
		{
			if (garbage != -1)
			{
				gp.get(i)[garbage] = sp[garbage];
			}
		}
		return gp;
	}
	/**
	 * @param listOfData List of Data for a particular Type
	 * @return Array of Strings consisting of the Specific Boundary of each type
	 */
	private String[] calculateSpecificRegion(ArrayList<ZooData> listOfData)
	{
		CountOfZooData all_counts = new CountOfZooData();
		String[] s = new String[16];
		for (int i = 0; i< 16; i++)
			s[i] = "";
		String parse;
		for (ZooData data: listOfData)
		{
			// Parse Hair to Unique
			parse = String.valueOf((data.hair)? 1: 0);
			if (!s[0].contains(parse)) {
				s[0] = s[0].concat(parse);
			}
			// Parse Feathers for Unique
			parse = String.valueOf((data.feathers)? 1: 0);
			if (!s[1].contains(parse)) {
				s[1] = s[1].concat(parse);
			}
			// Parse Eggs for Unique
			parse = String.valueOf((data.eggs)? 1: 0);
			if (!s[2].contains(parse)) {
				s[2] += (parse);
			}
			// Parse Milk for Unique
			parse = String.valueOf((data.milk)? 1: 0);
			if (!s[3].contains(parse)) {
				s[3] += (parse);
			}
			// Parse Airborne for Unique
			parse = String.valueOf((data.airborne)? 1: 0);
			if (!s[4].contains(parse)) {
				s[4] += (parse);
			}
			// Parse Aquatic for Unique
			parse = String.valueOf((data.aquatic)? 1: 0);
			if (!s[5].contains(parse)) {
				s[5] += (parse);
			}
			// Parse Predator for Unique
			parse = String.valueOf((data.predator)? 1: 0);
			if (!s[6].contains(parse)) {
				s[6] += (parse);
			}
			// Parse toothed for Unique
			parse = String.valueOf((data.toothed)? 1: 0);
			if (!s[7].contains(parse)) {
				s[7] += (parse);
			}
			// Parse backbone for Unique
			parse = String.valueOf((data.backbone)? 1: 0);
			if (!s[8].contains(parse)) {
				s[8] += (parse);
			}
			// Parse Breathes for Unique
			parse = String.valueOf((data.breathes)? 1: 0);
			if (!s[9].contains(parse)) {
				s[9] += (parse);
			}
			// Parse Venomous for Unique
			parse = String.valueOf((data.venomous)? 1: 0);
			if (!s[10].contains(parse)) {
				s[10] += (parse);
			}
			// Parse Fins for Unique
			parse = String.valueOf((data.fins)? 1: 0);
			if (!s[11].contains(parse)) {
				s[11] += (parse);
			}
			// Parse Legs for Unique
			parse = String.valueOf(data.legs);
			if (!s[12].contains(parse)) {
				s[12] += (parse);
			}
			// Parse Tail for Unique
			parse = String.valueOf((data.tail)? 1: 0);
			if (!s[13].contains(parse)) {
				s[13] += (parse);
			}
			// Parse Domestic for Unique
			parse = String.valueOf((data.domestic)? 1: 0);
			if (!s[14].contains(parse)) {
				s[14] += (parse);
			}
			// Parse CatSize for Unique
			parse = String.valueOf((data.catsize)? 1: 0);
			if (!s[15].contains(parse)) {
				s[15] += (parse);
			}
		}
		s[0] = (s[0].length() == all_counts.COUNT_HAIR)? "?" : s[0];
		s[1] = (s[1].length() == all_counts.COUNT_FEATHERS)? "?" : s[1];
		s[2] = (s[2].length() == all_counts.COUNT_EGGS)? "?" : s[2];
		s[3] = (s[3].length() == all_counts.COUNT_MILK)? "?" : s[3];
		s[4] = (s[4].length() == all_counts.COUNT_AIRBORNE)? "?" : s[4];
		s[5] = (s[5].length() == all_counts.COUNT_AQUATIC)? "?" : s[5];
		s[6] = (s[6].length() == all_counts.COUNT_PREDATOR)? "?" : s[6];
		s[7] = (s[7].length() == all_counts.COUNT_TOOTHED)? "?" : s[7];
		s[8] = (s[8].length() == all_counts.COUNT_BACKBONE)? "?" : s[8];
		s[9] = (s[9].length() == all_counts.COUNT_BREATHES)? "?" : s[9];
		s[10] = (s[10].length() == all_counts.COUNT_VENOMOUS)? "?" : s[10];
		s[11] = (s[11].length() == all_counts.COUNT_FINS)? "?" : s[11];
		s[12] = (s[12].length() == all_counts.COUNT_LEGS)? "?" : s[12];
		s[13] = (s[13].length() == all_counts.COUNT_TAIL)? "?" : s[13];
		s[14] = (s[14].length() == all_counts.COUNT_DOMESTIC)? "?" : s[14];
		s[15] = (s[15].length() == all_counts.COUNT_CATSIZE)? "?" : s[15];
		return s;
	}

	private void calculateAllSpecificBoundaries(ArrayList<ZooData> typeOne,ArrayList<ZooData> typeTwo,
												ArrayList<ZooData> typeThree,ArrayList<ZooData> typeFour,
												ArrayList<ZooData> typeFive, ArrayList<ZooData> typeSix,
												ArrayList<ZooData> typeSeven) throws IOException
	{
		String[] specificBoundaryFirstType,
				specificBoundarySecondType,
				specificBoundaryThirdType,
				specificBoundaryFourthType,
				specificBoundaryFifthType,
				specificBoundarySixthType,
				specificBoundarySeventhType;
		specificBoundaryFirstType = calculateSpecificRegion(typeOne);
		commonImplementor.writeSimpleArrayOfStrings(outputLogFile, specificBoundaryFirstType, 16, "Type 1");
		specificBoundarySecondType = calculateSpecificRegion(typeTwo);
		commonImplementor.writeSimpleArrayOfStrings(outputLogFile, specificBoundarySecondType, 16, "Type 2");
		specificBoundaryThirdType = calculateSpecificRegion(typeThree);
		commonImplementor.writeSimpleArrayOfStrings(outputLogFile, specificBoundaryThirdType, 16, "Type 3");
		specificBoundaryFourthType = calculateSpecificRegion(typeFour);
		commonImplementor.writeSimpleArrayOfStrings(outputLogFile, specificBoundaryFourthType, 16, "Type 4");
		specificBoundaryFifthType = calculateSpecificRegion(typeFive);
		commonImplementor.writeSimpleArrayOfStrings(outputLogFile, specificBoundaryFifthType, 16, "Type 5");
		specificBoundarySixthType = calculateSpecificRegion(typeSix);
		commonImplementor.writeSimpleArrayOfStrings(outputLogFile, specificBoundarySixthType, 16, "Type 6");
		specificBoundarySeventhType = calculateSpecificRegion(typeSeven);
		commonImplementor.writeSimpleArrayOfStrings(outputLogFile, specificBoundarySeventhType, 16, "Type 7");
	}
}
class MissingAttributeException extends Exception
{
	MissingAttributeException(String message) {
		super(message);
	}
}
