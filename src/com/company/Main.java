package com.company;
import java.io.File;
public class Main {

    private static void helpList()
    {
		System.out.println("Welcome to Machine Learning Assignment!!\n" +
				"There are few arguments required to execute this Project\n " +
				"1. -candidate-elimination" +
				"\n 2. -id3-algorithm\n");
    }
    public static void main(String[] args) throws Exception{
	    // List all Projects
        int len = args.length;
        if(len == 0)
        {
            System.out.printf("Arguments found %d. Please enter one argument to execute this program", len);
            return;
        }
        args[0] = args[0].toLowerCase();
        if (args[0].contains("?")) {
            helpList();
        }
        else {
            String s = args[0];
			String strFile;
            switch (s)
            {
                case "candidate-elimination":
                	System.out.println("Starting Candidate Elimination...");
                    CandidateElimination c = new CandidateElimination("data/zoo.txt");
                    strFile = c.startImplementation();
					System.out.println("Check " + strFile + " in logs Folder for Output Details!!");
                    break;
				case "id3-algorithm":
					System.out.println("Starting ID3 Algorithm Implementation...");
					File f = new File("data/adult_mod.txt");
					File outfile = new File("tests/adult_testDat_mod.txt");
					ID3Algorithm id3ALgo;
					if (f.exists()) {
						if (outfile.exists()) {
							id3ALgo = new ID3Algorithm("data/adult_mod.txt", "tests/adult_testDat_mod.txt");
						}
						else {
							id3ALgo = new ID3Algorithm("data/adult_mod.txt", "tests/adult_testData.txt");
						}
					} else {
						if (outfile.exists()) {
							id3ALgo = new ID3Algorithm("data/adult.txt", "tests/adult_testDat_mod.txt");
						} else {
							id3ALgo = new ID3Algorithm("data/adult.txt", "tests/adult_testData.txt");
						}
					}
                    strFile = id3ALgo.startImplementation();
					System.out.println("Check " + strFile + " in logs Folder for Output Details!!");
					break;
                default:
                    System.out.println("The Arguments did not match!");
            }
        }
    }
}
